public class App {
    public static void main(String[] args) throws Exception {
        Time time1 = new Time(2, 59, 59);
        Time time2 = new Time(4, 0, 0);
        System.out.println(time1);
        System.out.println(time2);
        System.out.println(" ");
        System.out.println("Tang time1 them 1 giay. time1 bay gio la:");
        time1.nextSecond();
        System.out.println(time1);
        System.out.println("Giam time2 di 1 giay. time2 bay gio la:");
        time2.previousSecond();
        System.out.println(time2);
    }
}
